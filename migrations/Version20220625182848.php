<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220625182848 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE announcement DROP FOREIGN KEY FK_4DB9D91C5845B26C');
        $this->addSql('DROP INDEX IDX_4DB9D91C5845B26C ON announcement');
        $this->addSql('ALTER TABLE announcement DROP job, DROP city_job, DROP category_job, CHANGE joob_id job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE announcement ADD CONSTRAINT FK_4DB9D91CBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_4DB9D91CBE04EA9 ON announcement (job_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE announcement DROP FOREIGN KEY FK_4DB9D91CBE04EA9');
        $this->addSql('DROP INDEX IDX_4DB9D91CBE04EA9 ON announcement');
        $this->addSql('ALTER TABLE announcement ADD job VARCHAR(255) NOT NULL, ADD city_job VARCHAR(255) NOT NULL, ADD category_job VARCHAR(255) NOT NULL, CHANGE job_id joob_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE announcement ADD CONSTRAINT FK_4DB9D91C5845B26C FOREIGN KEY (joob_id) REFERENCES job (id)');
        $this->addSql('CREATE INDEX IDX_4DB9D91C5845B26C ON announcement (joob_id)');
    }
}
