<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\AnnouncementRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\AnnouncementController;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AnnouncementRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['announcement']],
    itemOperations: [
        'get', 
        'patch',
        'update_amount_view' => [
            'method' => 'POST',
            'path' => '/announcements/{id}/upAmountView',
            'controller' => AnnouncementController::class,
        ]
    ],
    attributes: [
        "pagination_items_per_page" => 10, 
        "pagination_client_enabled" => false
    ],
    order: [
        "amountView" => "DESC"
    ]
)]
class Announcement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups('announcement')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $lastname;

    #[ORM\Column(type: 'boolean')]
    #[Groups('announcement')]
    private $onRemote;

    #[ORM\Column(type: 'integer')]
    #[Groups('announcement')]
    private $age;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $email;

    #[ORM\Column(type: 'text')]
    #[Groups('announcement')]
    private $description;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $picture;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $resume;

    #[ORM\Column(type: 'integer')]
    #[Groups('announcement')]
    private $amountView;

    #[ORM\Column(type: 'integer')]
    #[Groups('announcement')]
    private $amountWarning;
    
    #[ORM\ManyToOne(targetEntity: Job::class, inversedBy: 'announcements')]
    #[Groups('announcement')]
    private $job;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups('announcement')]
    private $city;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function isOnRemote(): ?bool
    {
        return $this->onRemote;
    }

    public function setOnRemote(bool $onRemote): self
    {
        $this->onRemote = $onRemote;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getAmountView(): ?int
    {
        return $this->amountView;
    }

    public function setAmountView(int $amountView): self
    {
        $this->amountView = $amountView;

        return $this;
    }

    public function getAmountWarning(): ?int
    {
        return $this->amountWarning;
    }

    public function setAmountWarning(int $amountWarning): self
    {
        $this->amountWarning = $amountWarning;

        return $this;
    }

    public function getJob(): ?Job
    {
        return $this->job;
    }

    public function setJob(?Job $job): self
    {
        $this->job = $job;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }
}
