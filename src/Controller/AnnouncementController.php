<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use App\Repository\AnnouncementRepository;
use App\Entity\Announcement;

#[AsController]
class AnnouncementController extends AbstractController
{
    private $announcementRepository;

    public function __construct(AnnouncementRepository $announcementRepository)
    {
        $this->announcementRepository = $announcementRepository;
    }

    public function __invoke(Announcement $data)
    {
        $amountView = $data->getAmountView();

        $data->setAmountView($amountView + 1);
        
        $this->announcementRepository->add($data);

        return $data;
    }
}